<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


if ( ! defined( 'DOMAIN')) define('DOMAIN','fashant.com');

if ( ! defined( 'APP')) define('APP','Fashant');

// tables

if ( ! defined( 'BRAND_USER')) define('BRAND_USER','users');
if ( ! defined( 'USER')) define('USER','users');
if ( ! defined( 'BRAND')) define('BRAND','brands');
if ( ! defined( 'CAT')) define('CAT','brand_categories');
if ( ! defined( 'BRAND_CAT')) define('BRAND_CAT','brand_in_category');
if ( ! defined( 'SALE')) define('SALE','brand_sale_offers');
if ( ! defined( 'PRICE_CAT')) define('PRICE_CAT','price_category');
if ( ! defined( 'REVIEW')) define('REVIEW','reviews');
if ( ! defined( 'REVIEW_LIKES')) define('REVIEW_LIKES','review_likes');
if ( ! defined( 'COMMENT')) define('COMMENT','comments');
if ( ! defined( 'FAV')) define('FAV','favorite_brands');
if ( ! defined( 'BRAND_GALLERY')) define('BRAND_GALLERY','brand_gallery');
if ( ! defined( 'RECOVERY')) define('RECOVERY','password_recovery_request');
if ( ! defined( 'OPEN_TIMES')) define('OPEN_TIMES','opening_hours');
if ( ! defined( 'NOTIF')) define('NOTIF','notifications');

/* PATHS */

// views

define('V_PG_BP','pages/');

define('V_SC_BP','sections/');

define('V_TPL','templates/');

define('V_MAIL','mail/');

// error views

define('V_ERROR','errors/html/');


// assets is common for all user types

//define('ASSETS', '..' . DIRECTORY_SEPARATOR . 'assets/brand_portal/');
define('ASSETS', 'assets/v1/');
define('ASSETS_IMG', ASSETS . 'images/');
define('ASSETS_CSS', ASSETS . 'css/');
define('ASSETS_JS', ASSETS . 'js/');

define('ASSETS_LOGIN', 'assets/login/');


/* END OF PATHS */


define('SITE_TITLE','Fashant | Brand Portal');

define('NOW',date('Y-m-d H:i:s'));

define('TODAY',date('Y-m-d'));

define('CURR',"$ ");


if( ENVIRONMENT == 'production') {
    define('URL_IMG','https://admin.fashant.com/uploads/brands/');
    define('URL_BRAND_IMG','https://admin.fashant.com/uploads/brands/');
    define('PATH_BRAND_LOGO','../../admin.fashant.com/html/uploads/brands/');
    define('PATH_BRAND_IMG','../../admin.fashant.com/html/uploads/brands/');
    // custom path form vendor folder
    define('PATH_VENDOR', '../../ci/vendor/' );
    define('USE_SMTP', true );
}
else {
    define('URL_IMG','https://admin.fashant.com/uploads/brands/');
    define('URL_BRAND_IMG','https://admin.fashant.com/uploads/brands/');
    define('PATH_BRAND_LOGO','../../admin.fashant.com/html/uploads/brands/');
    define('PATH_BRAND_IMG','../../admin.fashant.com/html/uploads/brands/');
    // custom path form vendor folder
    define('PATH_VENDOR', '../../ci/vendor/' );
    define('USE_SMTP', true );
}

define('PATH_UPLOAD_BRAND_IMG','assets/shop/uploads/products/');


// messages
define('ACCESS_DENIED', 'Access Denied !');



// Google API configuration
define('GOOGLE_CLIENT_ID', '721656441109-0ib05d4s4sfdhnubo7ce9hi11rfc0sna.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET', '5FcmoE3cREHepHyEJT3ONExt');
define('GOOGLE_REDIRECT_URL', 'https://' . $_SERVER['HTTP_HOST'] . '/auth/googleLogin');


// Facebook API configuration
define('FB_APP_ID', '543610996167005');
define('FB_APP_SECRET', 'c16025c5613bf5a95770583197c6f9e4');
define('FB_REDIRECT_URL', 'https://' . $_SERVER['HTTP_HOST'] . '/auth/facebookLogin');