<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * This function is used to print the content of any data
 */
function pre($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function prepVars($ar)
{
    $data = array();

    foreach($ar as $k => $v)
    {
        $data[$k] = $v;
    }

    return $data;
}

if(!function_exists('customerStatusLabels'))
{
    function customerStatusLabels($status) {

        if($status == 0) return '<div class="text-orange">Not-verified</div>';

        if($status == 1) return '<div class="text-green">Verified</div>';

        if($status == 2) return '<div class="text-orange">Blocked</div>';
    }
}
if(!function_exists('glueArray'))
{
    function glueArray($arr) {

        return "'" . implode("','", $arr) . "'";
    }
}

if(!function_exists('toArray'))
{
    function toArray($res) {

        //$res -> next_result();

        $data = array();
        foreach ($res->result_array() as $row)
        {
            array_push($data,$row);
        }
        //$res -> free_result();
        return $data;
    }
}

if(!function_exists('toRow'))
{
    function toRow($res) {

        $row = $res -> result_array($res);
        return $row[0];
    }
}
if(!function_exists('toOneDimArray'))
{
    function toOneDimArray($res,$field) {
        $data = array();
        foreach ($res as $row)
        {
            array_push($data,$row -> $field);
        }
        return $data;
    }
}
if(!function_exists('prepAlias'))
{
    function prepAlias($str) {

        $str = explode(" ", $str);
        return implode("-", $str);
    }
}

if(!function_exists('prepPostVar'))
{
    function prepPostVar($vars) {

        $dt = array();
        foreach($vars as $k => $v)
        {
            $dt[$k] = $v;
        }
        return $dt;
    }
}

/**
 * This function used to get the CI instance
 */
if(!function_exists('get_instance'))
{
    function get_instance()
    {
        $CI = &get_instance();
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 */
if(!function_exists('getHashedPassword'))
{
    function getHashedPassword($plainPassword)
    {
        return password_hash($plainPassword, PASSWORD_DEFAULT);
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 * @param {string} $hashedPassword : This is hashed password
 */
if(!function_exists('verifyHashedPassword'))
{
    function verifyHashedPassword($plainPassword, $hashedPassword)
    {
        return password_verify($plainPassword, $hashedPassword) ? true : false;
    }
}

/**
 * This method used to get current browser agent
 */
if(!function_exists('getBrowserAgent'))
{
    function getBrowserAgent()
    {
        $CI = get_instance();
        $CI->load->library('user_agent');

        $agent = '';

        if ($CI->agent->is_browser())
        {
            $agent = $CI->agent->browser().' '.$CI->agent->version();
        }
        else if ($CI->agent->is_robot())
        {
            $agent = $CI->agent->robot();
        }
        else if ($CI->agent->is_mobile())
        {
            $agent = $CI->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }

        return $agent;
    }
}

if(!function_exists('setProtocol'))
{
    function setProtocol()
    {
        $CI = &get_instance();

        $CI->load->library('email');

        $config['protocol'] = PROTOCOL;
        $config['mailpath'] = MAIL_PATH;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $CI->email->initialize($config);

        return $CI;
    }
}

if(!function_exists('emailConfig'))
{
    function emailConfig()
    {
        $CI->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['mailpath'] = MAIL_PATH;
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
    }
}

if(!function_exists('resetPasswordEmail'))
{
    function resetPasswordEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;

        $CI = setProtocol();

        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("Reset Password");
        $CI->email->message($CI->load->view('email/resetPassword', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();

        return $status;
    }
}

if(!function_exists('setFlashData'))
{
    function setFlashData($status, $flashMsg)
    {
        $CI = get_instance();
        $CI->session->set_flashdata($status, $flashMsg);
    }
}

if(!function_exists('genUuid'))
{
    function genUuid(){
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}

if(!function_exists('my_escapeshellarg'))
{
    /* custom function for php's escapeshellarg() function.. disabled in live servers for security reasons */
    function my_escapeshellarg($input)
    {
        $input = str_replace('\'', '\\\'', $input);

        return '\''.$input.'\'';
    }
}

if(!function_exists('shopPaginate'))
{
    function shopPaginate($pagination_link,$total_record,$rows_per_page,$current_page)
    {
        /* pagnation shit */

        $num_of_btns = 7;

        $pagination = '';

        if($total_record >= $rows_per_page) {

            //$pagination_link = base_url().'/admin/prod/listing/';

            $total_pages = ceil($total_record / $rows_per_page);

            $pagination = '<div class="page-nav center">';

            if($current_page > 1) {
                //$pagination .= '<li class="jplist-first" data-number="0" data-type="first"><a href="'.$pagination_link.'1"><span aria-hidden="true">«</span><span class="sr-only">First</span></a></li>';
                //$pagination .= '<li class="jplist-prev" data-type="prev" data-number="0"><a href="'.$pagination_link.($current_page-1).'"><span aria-hidden="true">‹</span><span class="sr-only">Previous</span></a></li>';

                $pagination .= '<div class="page-nav-item"><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></div>';
            }

            for($i = 1;$i <= $total_pages;$i++) {

                if($i >= ($current_page - floor($num_of_btns/2)) && $i <= ($current_page + floor($num_of_btns/2))) {
                    //$pagination .= '<li data-type="page" ';

                    $class_active = "";
                    $class_current = "";

                    if($i == $current_page)
                        $class_active = "active";

                    // $pagination .= ' data-number="'.($i-1).'">';

                    if($i != $current_page)
                        $pagination .= '<div class="page-nav-item"><a href="'.$pagination_link.$i.'">'.$i.'</a></div>';
                    else
                        $pagination .= '<div class="page-nav-item '.$class_active.'">'.$i.'</div>';

                    // $pagination .= '</li>';
                }
            }

            if($current_page < $total_pages) {
                //$pagination .= '<li class="jplist-next" data-type="next" data-number="1"><a href="'.$pagination_link.($current_page+1).'"><span aria-hidden="true">›</span><span class="sr-only">Next</span></a></li>';
                //$pagination .= '<li class="jplist-last" data-type="last" data-number="6"><a href="'.$pagination_link.($total_pages).'"><span aria-hidden="true">»</span><span class="sr-only">Last</span></a></li>';
                $pagination .= '<div class="page-nav-item"><a href="'.$pagination_link.($i+1).'"><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>';
            }


            //$pagination .= '</ul>';
            //$pagination .= '<div style="margin-top:-15px;"><span>Page '.$current_page.' of '.$total_pages.'</span></div>';
            $pagination .= '</div>';

            return $pagination;
        }
        /* end pagnation shit */
    }
}

if(!function_exists('time_elapsed_string')) {
    function time_elapsed_string($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}

if(!function_exists('prepPassword')) {
    function prepPassword($string)
    {
        return password_hash($string, PASSWORD_DEFAULT);
    }
}

if(!function_exists('randomPassword')) {
    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}

if(!function_exists('raiseAjaxError')) {
    function raiseAjaxError() {

        return true;
    }
}

if(!function_exists('userStatusLabel')) {

    function userStatusLabel($status) {

        if($status == '0')
            return '<span class="badge badge-danger">Blocked</span>';

        else if($status == '1')
            return '<span class="badge badge-success">Active</span>';

//        else if($status == '2')
//            return '<span class="badge danger">Blocked</span>';

    }
}

if(!function_exists('projectStatusLabel')) {

    function projectStatusLabel($status) {

        if($status == '0')
            return '<span class="badge badge-danger">Pending</span>';

        else if($status == '1')
            return '<span class="badge badge-success">Active</span>';

        else if($status == '2')
            return '<span class="badge danger">Blocked</span>';

        else if($status == '3')
            return '<span class="badge danger">Closed</span>';

    }
}

if(!function_exists('boolStatusLabel')) {

    function boolStatusLabel($status) {

        if($status == '0')
            return '<span class="badge badge-warning">No</span>';

        else if($status == '1')
            return '<span class="badge badge-success">Yes</span>';

//        else if($status == '2')
//            return '<span class="badge danger">Blocked</span>';

    }
}

if(!function_exists('boolRequestStatusLabel')) {

    function boolRequestStatusLabel($status) {

        if($status == '0')
            return '<span class="badge badge-warning">Pending</span>';

        else if($status == '1')
            return '<span class="badge badge-success">Accepted</span>';

        else if($status == '2')
            return '<span class="badge badge-danger">Rejected</span>';

    }
}

if(!function_exists('boolApprovalStatusLabel')) {

    function boolApprovalStatusLabel($status) {

        if($status == '0')
            return '<span class="badge badge-warning">Pending</span>';

        else if($status == '1')
            return '<span class="badge badge-success">Approved</span>';

        else if($status == '2')
            return '<span class="badge badge-danger">Rejected</span>';

    }
}

if(!function_exists('boolAvailStatusLabel')) {

    function boolAvailStatusLabel($status) {

        if($status == '0')
            return '<span class="badge badge-warning">Not Available</span>';

        else if($status == '1')
            return '<span class="badge badge-success">Available</span>';

//        else if($status == '2')
//            return '<span class="badge danger">Blocked</span>';

    }
}

/*function csrfField()
{
    echo "<input type=\"hidden\" name=\" ".$this->security->get_csrf_token_name() ." \" value=\" ". $this->security->get_csrf_hash() ." \"/>";
}

function initJsCsrf()
{
    echo "
            <script>
                var csrf_name = '". $this->security->get_csrf_token_name() ."';
                var csrf_val = '". $this->security->get_csrf_hash() ."';
            </script>
         ";
}*/

function analytics()
{
    $gid = 'UA-130582519-1';
    echo '
        <script async src="https://www.googletagmanager.com/gtag/js?id='.$gid.'"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag(\'js\', new Date());
    
            gtag(\'config\', \''.$gid.'\');
        </script>
    ';
}

function comTypeLabel($comType)
{
    if($comType == '2') {
        return "Lender";
    }
    else if($comType == '3') {
        return "Borrower";
    }
    else if($comType == '2::3') {
        return "Lender/Borrower";
    }
    else
        return false;
}

function makeComTypeLabel(array $comTypeLabel)
{
    return implode(" / ", $comTypeLabel);
}

// takes in datetime format
function prepDate(int $format, string $datetime)
{
    switch ($format)
    {
        case 1:
            $fm = 'j M, Y';
            break;

        case 2:
            $fm = 'Y-m-d';
            break;

        case 3:
            $fm = 'j M, Y g:iA';
            break;

        case 4:
            $fm = 'j M g:iA';
            break;

        default:
            $fm = 'Y-m-d';
            break;
    }

    return date($fm, strtotime($datetime) );
}

function custom_current_url() {
    return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

function getHoursOptions($val)
{
    if ($val == "") {
        echo "<option value='$val' selected></option>";
    } else {
        echo "<option value='' disabled></option>";
    }

    $timings = array(
        "Closed",
        "06:00 am",
        "06:30 am",
        "07:00 am",
        "07:30 am",
        "08:00 am",
        "08:30 am",
        "09:00 am",
        "09:30 am",
        "10:00 am",
        "10:30 am",
        "11:00 am",
        "11:30 am",
        "12:00 pm",
        "12:30 pm",
        "13:00 pm",
        "13:30 pm",
        "14:00 pm",
        "14:30 pm",
        "15:00 pm",
        "15:30 pm",
        "16:00 pm",
        "16:30 pm",
        "17:00 pm",
        "17:30 pm",
        "18:00 pm",
        "18:30 pm",
        "19:00 pm",
        "19:30 pm",
        "20:00 pm",
        "20:30 pm",
        "21:00 pm",
        "21:30 pm",
        "22:00 pm",
        "22:30 pm",
        "23:00 pm",
        "23:30 pm",
    );

    foreach( $timings as $items ) {
        $selected = "";
        if( $val != "" ) {
            if ($items == $val) {
                $selected = "selected";
            }
        }
        echo "<option value='$items' $selected >$items</option>";
    }

}

?>