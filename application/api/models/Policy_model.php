<?php
class Policy_model extends MY_Model {

    private $tbl = BRAND;

    public function getMyBrandPolicy( $pm )
    {
        $where = '';
        $bind = array();

        if( isset($pm['brand_id']) && $pm['brand_id'] > 0 )
        {
            $where .= "AND ".BRAND.".id=?";
            array_push($bind, $pm['brand_id'] );
        }

        if( isset($pm['brand_user']) && $pm['brand_user'] > 0 )
        {
            $where .= "AND ".BRAND.".ukey=?";
            array_push($bind, $pm['brand_user'] );
        }

        $sql = "
            SELECT 
                ".BRAND.".*
            FROM ".BRAND." 
            WHERE 1=1 
                $where 
        ";

        $res = $this -> db -> query( $sql, $bind );

        if($res -> num_rows() > 0)
            return toRow($res);

        return false;
    }

}
