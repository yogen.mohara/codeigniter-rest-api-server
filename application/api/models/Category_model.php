<?php
class Category_model extends MY_Model {

    private $tbl = CAT;

    public function getRow($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        $res = $this -> db -> get($this -> tbl);
        if( $res -> num_rows() > 0 )
            return toRow($res);
        return false;
    }

    public function getSecondary()
    {
        $this -> db -> select('*');
        $this -> db -> where("primary_category",0);
        $this -> db -> order_by("category");
        $res = $this -> db -> get($this -> tbl);
        if( $res -> num_rows() > 0 )
            return toArray($res);
        return false;
    }

}
