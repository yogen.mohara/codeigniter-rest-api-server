<?php
class Brand_gallery_model extends MY_Model {

    private $tbl = BRAND_GALLERY;

    public function get($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("brand_id",$id);
        $this -> db -> group_by("file_name");

        $res = $this -> db -> get($this -> tbl);
        if( $res -> num_rows() > 0 )
            return toArray($res);

        return false;
    }

}
