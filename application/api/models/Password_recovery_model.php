<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Password_recovery_model extends MY_Model
{

    private $tbl = RECOVERY;

    public function getRow($token)
    {
        if(!$token) return false;

        $this -> db -> select('*');
        $this -> db -> where("token",$token);
        $this -> db -> where("used",1);
        $this -> db -> where("expires_on >=",NOW);
        $res = $this -> db -> get($this -> tbl);
        return toRow($res);
    }

    public function validateToken($token)
    {
        if(!$token) return false;

        $this -> db -> select('*');
        $this -> db -> where("token",$token);
        $this -> db -> where("used",0);
        $this -> db -> where("expires_on >=",NOW);

        $res = $this -> db -> get($this -> tbl);

        // echo $this -> db -> last_query();

        if ($res->num_rows() > 0)
        {
            $row = $res->result_array();
            return $row[0];
        }

        return false;
    }

    public function changeStatus($id,$status)
    {
        $data = array(
            'status' => $status,
        );

        $this -> db -> where('user_id', $id);
        return $this -> db -> update($this -> tbl, $data);
    }

    public function insert($pm)
    {
        return $lid = $this -> ins(
            $this -> tbl,
            $pm
        );
    }

    public function update($pm,$pm_cond)
    {
        return $this -> upd(
            $this -> tbl,
            $pm,
            $pm_cond
        );
    }


}

