<?php
class Promo_model extends MY_Model {

    private $tbl = SALE;

    public function getBrandPromo( $brand_id )
    {
        $bind = array();

        $sql = "
            SELECT 
                ".SALE.".*
            FROM ".SALE." 
            WHERE 1=1 
                AND ".SALE.".brand_id=?
        ";
        array_push($bind, $brand_id );

        $res = $this -> db -> query( $sql, $bind );

        if($res -> num_rows() > 0)
            return toRow($res);

        return false;
    }

}
