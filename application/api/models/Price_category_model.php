<?php
class Price_category_model extends MY_Model {

    private $tbl = PRICE_CAT;

    public function getRow($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        $res = $this -> db -> get($this -> tbl);
        if( $res -> num_rows() > 0 )
            return toRow($res);
        return false;
    }

}
