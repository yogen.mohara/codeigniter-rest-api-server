<?php
class Favourite_model extends MY_Model {

    private $tbl = FAV;

    public function getRow($ukey, $brand_id)
    {

        $this -> db -> select('*');
        $this -> db -> where("ukey",$ukey);
        $this -> db -> where("brand_id",$brand_id);
        $res = $this -> db -> get($this -> tbl);
        if( $res -> num_rows() > 0 )
            return toRow($res);
        return false;
    }

    public function getBrandByUser( $ukey )
    {
        $bind = array();

        $sql = "
            SELECT 
                ".FAV.".*,
                ".BRAND.".brand as brand_name,
                ".BRAND.".shopping_center,
                concat( ".USER.".first_name, ' ', ".USER.".last_name ) as user_full_name 
            FROM ".FAV." 
            JOIN ".BRAND." ON ".BRAND.".id=".FAV.".brand_id  
            JOIN ".USER." ON ".USER.".id=".FAV.".ukey  
            WHERE 1=1 
                AND ".FAV.".ukey=? 
            ORDER BY brand_name  
        ";
        array_push($bind, $ukey );

        $res = $this -> db -> query( $sql, $bind );

        if($res -> num_rows() > 0)
            return toArray($res);

        return false;
    }

}
