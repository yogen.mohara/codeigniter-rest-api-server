<?php
class Brand_user_model extends MY_Model {

    private $tbl = BRAND_USER;

    public function get()
    {
        $this -> db -> select("*");
        //$this -> db -> where("user_type","1");
        $this -> db -> where("is_del","0");
        $this -> db -> where("user_type","Brand");
        // $this -> db -> order_by("created_on desc");
        $res = $this -> db -> get($this -> tbl);
        return toArray($res);
    }

    public function getRow($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        $this -> db -> where("user_type","Brand");
        // $this -> db -> where("is_del",'0');
        $res = $this -> db -> get($this -> tbl);
        return toRow($res);
    }

    public function checkId($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        $res = $this -> db -> get($this -> tbl);
        return $res -> num_rows();
    }

    public function checkSmUser( $oauth_provider, $oauth_id, $email )
    {

        $this -> db -> select('*');
        $this -> db -> where("oauth_provider",$oauth_provider);
        $this -> db -> where("oauth_uid",$oauth_id);
        $this -> db -> where("email",$email);
        $this -> db -> where("user_type","Brand");
        // $this -> db -> where("is_del",'0');
        $res = $this -> db -> get($this -> tbl);
        if( $res -> num_rows() == 0 ) return false;
        else return toRow($res);
    }

    public function validateToken( $id, $token )
    {

        $this -> db -> select('*');
        $this -> db -> where("hash",$token);
        $this -> db -> where("id",$id);
        $this -> db -> where("active",0);
        $this -> db -> where("email_verified",0);
        $this -> db -> where("user_type","Brand");
        // $this -> db -> where("is_del",'0');
        $res = $this -> db -> get($this -> tbl);
        return $res -> num_rows();
    }

    public function validatePassword($id, $password )
    {

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        // $this -> db -> where("is_del",0);
        $res = $this -> db -> get($this -> tbl);
        if( $res -> num_rows() == 0 ) return false;
        $row = toRow($res);
        $current_password = $row['password'];
        if( password_verify( $password, $current_password ) ) return true;
        return false;
    }

    public function validateLogin($username,$password)
    {
        $login_stats = array(
            'login' => true,
            'password' => true,
            'username' => true,
            'status' => true,
            'msg' => '',
            'user' => ''
        );

        $user = $this -> getByEmail($username);
        //pre($user);
        //pre($username);
        //pre($password);

        if( ! is_array($user) )
        {
            $login_stats['login'] = false;
            $login_stats['username'] = false;
            $login_stats['msg'] = 'Invalid Username';
            return $login_stats;
        }

        if( ! password_verify( $password, $user['password'] ) )
        {
            $login_stats['login'] = false;
            $login_stats['password'] = false;
            $login_stats['msg'] = 'Invalid Password';
            return $login_stats;
        }

        if( $user['active'] != '1' )
        {
            $login_stats['login'] = false;
            $login_stats['status'] = false;
            $login_stats['msg'] = 'Invalid Account Status';
            return $login_stats;
        }

        $login_stats['user'] = $user;

        return $login_stats;
    }

    public function getByEmail($email)
    {
        if(!$email) return false;

        $this -> db -> select('*');
        $this -> db -> where("email",$email);
        // $this -> db -> where("is_del",'0');
        $res = $this -> db -> get($this -> tbl);
        //echo $this -> db -> last_query();

        if ($res->num_rows() > 0)
        {
            return toRow($res);
        }
        return false;
    }

    public function changeStatus($id,$status)
    {
        $data = array(
            'status' => $status,
        );

        $this -> db -> where('id', $id);
        return $this -> db -> update($this -> tbl, $data);
    }

    public function softDelete($id)
    {
        return $this -> upd(
            $this -> tbl,
            array(
                'is_del' => '1'
            ),
            array(
                'id' => $id
            )
        );
    }

    public function delete($id)
    {
        return $this -> del($this -> tbl, array('id' => $id) );
    }

    public function insert($data) {
        return $this -> db -> insert($this -> tbl, $data);
    }

}
