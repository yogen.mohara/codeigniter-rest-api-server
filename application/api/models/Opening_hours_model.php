<?php
class Opening_hours_model extends MY_Model {

    private $tbl = OPEN_TIMES;

    public function getRow($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        $res = $this -> db -> get($this -> tbl);

        if($res -> num_rows() > 0)
            return toRow($res);

        return false;
    }

}
