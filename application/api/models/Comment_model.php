<?php
class Comment_model extends MY_Model {

    private $tbl = REVIEW;

    public function getReviewComments( $review_id )
    {
        $bind = array();

        $sql = "
            SELECT 
                ".COMMENT.".*,
                concat( ".USER.".first_name, ' ', ".USER.".last_name ) as user_full_name 
            FROM ".COMMENT." 
            JOIN ".REVIEW." ON ".REVIEW.".id=".COMMENT.".review_id  
            JOIN ".USER." ON ".USER.".id=".COMMENT.".user_id  
            WHERE 1=1 
                AND ".COMMENT.".review_id=? 
                AND ".REVIEW.".is_verified=1 
            ORDER BY ".COMMENT.".id desc  
        ";
        array_push($bind, $review_id );

        $res = $this -> db -> query( $sql, $bind );

        if($res -> num_rows() > 0)
            return toArray($res);

        return false;
    }

}
