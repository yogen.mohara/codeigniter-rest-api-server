<?php
class Brand_cat_model extends MY_Model {

    private $tbl = BRAND_CAT;

    public function get($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("brand_id",$id);
        $res = $this -> db -> get($this -> tbl);
        return toArray($res);
    }

    public function getCategories($mode) {


        $this -> db -> select('*');
        $mode == 'primary' ? $this -> db -> where("primary_category",1) : $mode == 'secondary' ? $this -> db -> where("primary_category",0) : '' ;
        $this -> db -> order_by("category");
        $res = $this -> db -> get(CAT);
        return toArray($res);
    }

    public function getAssinedPrimaryCategories($brand_id)
    {
        $sql = "
            select 
                c.id as brand_id,
                c.id as category_id,
                c.category as category_name                
            from ".CAT." as c 
            join ".BRAND_CAT." as bc on c.id = bc.category_id
            where 
                bc.brand_id = ?
                and bc.primary_tagging=0 
                and c.primary_category=1
            order by c.id            
        ";

        $res = $this -> db -> query( $sql, array( $brand_id ) );
        return toArray($res);
    }

    public function getAssinedTaggings($brand_id)
    {
        $sql = "
            select 
                c.id as brand_id,
                c.id as category_id,
                c.category as category_name                
            from ".CAT." as c 
            join ".BRAND_CAT." as bc on c.id = bc.category_id
            where 
                bc.brand_id = ?
                and c.primary_category=0 
            order by bc.primary_tagging desc,  category_name           
        ";

        $res = $this -> db -> query( $sql, array( $brand_id ) );
        return toArray($res);
    }



}
