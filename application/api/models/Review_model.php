<?php
class Review_model extends MY_Model {

    private $tbl = REVIEW;

    public function getBrandReviews( $brand_id )
    {
        $bind = array();

        $sql = "
            SELECT 
                ".REVIEW.".*,
                ".BRAND.".brand as brand_name,
                (
                    SELECT COUNT(id) 
                    FROM ".COMMENT." 
                    WHERE ".COMMENT.".review_id=".REVIEW.".id
                ) as comments_count,
                (
                    SELECT COUNT(id) 
                    FROM ".REVIEW_LIKES." 
                    WHERE ".REVIEW_LIKES.".review_id=".REVIEW.".id
                ) as likes_count,
                concat( ".USER.".first_name, ' ', ".USER.".last_name ) as user_full_name 
            FROM ".REVIEW." 
            JOIN ".BRAND." ON ".BRAND.".id=".REVIEW.".brand_id  
            JOIN ".USER." ON ".USER.".id=".REVIEW.".ukey  
            WHERE 1=1 
                AND ".REVIEW.".brand_id=? 
                AND ".REVIEW.".is_verified=1 
            ORDER BY ".REVIEW.".id desc  
        ";
        array_push($bind, $brand_id );

        $res = $this -> db -> query( $sql, $bind );

        if($res -> num_rows() > 0)
            return toArray($res);

        return false;
    }

    public function getRow($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        $res = $this -> db -> get($this -> tbl);
        return toRow($res);
    }

    public function validateOwner($id,$ukey)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        $this -> db -> where("ukey",$ukey);
        $res = $this -> db -> get($this -> tbl);
        return $res -> num_rows();
    }

    public function getUserReviews( $ukey )
    {
        $bind = array();

        $sql = "
            SELECT 
                ".REVIEW.".id,
                ".REVIEW.".ukey,
                ".REVIEW.".brand_id,
                ".REVIEW.".review,
                ".REVIEW.".rating,
                ".REVIEW.".date,
                ".REVIEW.".like_count,
                ".BRAND.".brand as brand_name,
                ".BRAND.".shopping_center,
                concat( ".USER.".first_name, ' ', ".USER.".last_name ) as user_full_name 
            FROM ".REVIEW." 
            JOIN ".BRAND." ON ".BRAND.".id=".REVIEW.".brand_id  
            JOIN ".USER." ON ".USER.".id=".REVIEW.".ukey  
            WHERE 1=1 
                AND ".REVIEW.".ukey=? 
                AND ".REVIEW.".is_verified=1 
            ORDER BY ".REVIEW.".id desc  
        ";
        array_push($bind, $ukey );

        $res = $this -> db -> query( $sql, $bind );

        if($res -> num_rows() > 0)
            return toArray($res);

        return false;
    }

}
