<?php
class User_model extends MY_Model {

    private $tbl = USER;

    public function get()
    {
        $this -> db -> select("*");
        $this -> db -> where("is_del","0");
        $this -> db -> order_by("id desc");
        $res = $this -> db -> get($this -> tbl);
        return toArray($res);
    }

    public function getRow($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        $res = $this -> db -> get($this -> tbl);
        return toRow($res);
    }

    public function validatePassword($id, $password )
    {

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        // $this -> db -> where("is_del",0);
        $res = $this -> db -> get($this -> tbl);
        if( $res -> num_rows() == 0 ) return false;
        $row = toRow($res);
        $current_password = $row['password'];
        if( password_verify( $password, $current_password ) ) return true;
        return false;
    }

    public function validateLogin($username,$password)
    {
        $login_stats = array(
            'login' => true,
            'password' => true,
            'username' => true,
            'status' => true,
            'message' => '',
            'user' => ''
        );

        $user = $this -> getByEmail($username);

        if( ! is_array($user) )
        {
            $login_stats['login'] = false;
            $login_stats['username'] = false;
            $login_stats['message'] = 'Invalid Username / Password.';
            return $login_stats;
        }

        if( ! password_verify( $password, $user['password'] ) )
        {
            $login_stats['login'] = false;
            $login_stats['password'] = false;
            $login_stats['message'] = 'Invalid Username / Password.';
            return $login_stats;
        }

        if( $user['active'] != '1' )
        {
            $login_stats['login'] = false;
            $login_stats['status'] = false;
            $login_stats['message'] = 'Invalid Account Status';
            return $login_stats;
        }

        $login_stats['user'] = $user;

        return $login_stats;
    }

    public function getByEmail($email)
    {
        if(!$email) return false;

        $this -> db -> select('*');
        $this -> db -> where("email",$email);
        $res = $this -> db -> get($this -> tbl);

        if ($res->num_rows() > 0)
        {
            return toRow($res);
        }
        return false;
    }

    public function changeStatus($id,$status)
    {
        $data = array(
            'active' => $status,
        );

        $this -> db -> where('id', $id);
        return $this -> db -> update($this -> tbl, $data);
    }

}
