<?php
class Review_like_model extends MY_Model {

    private $tbl = REVIEW_LIKES;

    public function getRow($id)
    {
        if(!$id) return false;

        $this -> db -> select('*');
        $this -> db -> where("id",$id);
        $res = $this -> db -> get($this -> tbl);
        return toRow($res);
    }

    public function validateOwner($id,$ukey)
    {

        $this -> db -> select('*');
        $this -> db -> where("review_id",$id);
        $this -> db -> where("user_id",$ukey);
        $res = $this -> db -> get($this -> tbl);
        return $res -> num_rows();
    }

}
