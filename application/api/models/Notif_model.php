<?php
class Notif_model extends MY_Model {

    private $tbl = NOTIF;

    public function getRow( $id )
    {

        $this -> db -> select('*');
        $this -> db -> where("not_id",$id);
        $res = $this -> db -> get($this -> tbl);
        if( $res -> num_rows() > 0 )
            return toRow($res);
        return false;
    }

    public function getByUser( $ukey )
    {
        $bind = array();

        $sql = "
            SELECT 
                ".NOTIF.".*
            FROM ".NOTIF." 
            WHERE 1=1 
                AND ".NOTIF.".user_id=? 
                AND ".NOTIF.".is_read=0 
            ORDER BY ".NOTIF.".not_id DESC
        ";
        array_push($bind, $ukey );

        $res = $this -> db -> query( $sql, $bind );

        if($res -> num_rows() > 0)
            return toArray($res);

        return false;
    }

}
