<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Brand extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {

        ini_set('display_errors', 1);

        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->methods['flogin_post']['limit'] = 50; // 50 requests per hour per user/key

        //$this -> load -> library("session");
        $this -> load -> helper("my_helper");
        //$this -> load -> model("brand_model");


    }

    public function listing_get()
    {
        // var_dump($this->session->userdata());

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === null)
        {
            // Invalid id, set the response and exit.
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $listing = null;
        $this -> load -> library("Fs_Brand");
        $listing = $this -> fs_brand -> get($id);

        if (!empty($listing))
        {
            $this->set_response($listing, 200); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => false,
                'message' => 'Listing could not be found'
            ], 404); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function reviews_get()
    {

        $id = $this->get('brand_id');

        // Validate the id.
        if ($id === null)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $listing = null;

        $this -> load -> library("Fs_Brand", array( 'id' => $id ) );
        $this -> fs_brand -> getReviews();
        
        $listing['rating'] = $this -> fs_brand -> rating;
        $listing['reviews_count'] = $this -> fs_brand -> reviews_count;
        $listing['reviews'] = $this -> fs_brand -> reviews;

        if (!empty($listing))
        {
            $this->set_response($listing, 200); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => false,
                'message' => "Reviews could not be found for brand id: $id"
            ], 404); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function logo_get()
    {

        $id = $this->get('id');

        // Validate the id.
        if ($id === null)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $listing = null;

        $this -> load -> library("Fs_Brand", array( 'id' => $id ) );
        $this -> fs_brand -> getBrandLogo();

        $listing['logo'] = $this -> fs_brand -> logo;

        if (!empty($listing))
        {
            $this->set_response($listing, 200); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => false,
                'message' => "Reviews could not be found for brand id: $id"
            ], 404); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function check_favourited_get()
    {

        $id = $this->get('id');
        $ukey = $this->get('ukey');

        // Validate the id.
        if ($id === null || $ukey === null)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $id = (int) $id;
        $ukey = (int) $ukey;

        // Validate the id.
        if ($id <= 0 || $ukey <= 0)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $listing = null;

        $this -> load -> library("Fs_Brand", array( 'id' => $id ) );
        $res = $this -> fs_brand -> checkFavourited( $ukey );

        $listing = array(
            'is_favourited' => "false"
        );

        if( $res ) {
            $listing = array(
                'is_favourited' => "true"
            );
        }

        if (!empty($listing))
        {
            $this->set_response($listing, 200); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => false,
                'message' => "Reviews could not be found for brand id: $id"
            ], 404); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function yogen_post() {
        $this->set_response($_POST, 200); // OK (200) being the HTTP response code
    }

}
