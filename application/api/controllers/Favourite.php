<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Favourite extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {

        ini_set('display_errors', 1);

        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['by_user_get']['limit'] = 500; // 500 requests per hour per user/key

        $this -> load -> helper("my_helper");

    }

    public function by_user_get()
    {

        // user id
        $id = $this->get('id');

        if ($id === null)
        {
            // Invalid id, set the response and exit.
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $listing = null;
        $this -> load -> library("Fs_Favourite");
        $a = $this -> fs_favourite -> getFavouritedBrands($id);
        $listing['favourite_brands_count'] = is_array( $a ) ? count( $a ) : '0';
        $listing['favourite_brands'] = is_array( $a ) && count( $a ) > 0 ? $a : null;
        unset($a);

        if (!empty($listing))
        {
            $this->set_response($listing, 200); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => false,
                'message' => 'Favourite Brands could not be found'
            ], 404); // NOT_FOUND (404) being the HTTP response code
        }
    }

}
