<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Review extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {

        ini_set('display_errors', 1);

        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key

        $this -> load -> helper("my_helper");

    }

    /*
    returns all reviews posted by a user id
    */
    public function user_get()
    {

        $id = $this->get('ukey');

        // Validate the id.
        if ($id === null)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $listing = null;

        $this -> load -> library("Fs_Review" );
        $a = $this -> fs_review -> getUserReviews( $id );

        $listing['reviews_count'] = is_array( $a ) ? count( $a ) : '0';
        $listing['reviews'] = is_array( $a ) && count( $a ) > 0 ? $a : null;
        unset($a);

        if (!empty($listing))
        {
            $this->set_response($listing, 200); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => false,
                'message' => "Reviews could not be found for user id: $id"
            ], 404); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /*
    returns all reviews posted for a brand
    */
    public function brand_get()
    {

        $id = $this->get('brand_id');

        // Validate the id.
        if ($id === null)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            $this->response(null, 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $listing = null;

        $this -> load -> library("Fs_Review" );
        $a = $this -> fs_review -> getBrandReviews( $id );

        $listing['reviews_count'] = is_array( $a ) ? count( $a ) : '0';
        $listing['reviews'] = is_array( $a ) && count( $a ) > 0 ? $a : null;
        unset($a);

        if (!empty($listing))
        {
            $this->set_response($listing, 200); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => false,
                'message' => "Reviews could not be found for user id: $id"
            ], 404); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function index_delete() {

        $id = (int) $this->input->get('id', true);
        $ukey = (int) $this->input->get('ukey', true);

        // Validate the id.
        if ($id <= 0 || $ukey <= 0)
        {
            $this->response(array('message' => 'Invalid Parameters'), 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $response = array(
            'status' => false
        );

        $this -> load -> library("Fs_Review" );
        $del = $this -> fs_review -> delete( $id, $ukey );

        if( $del ) {
            $response['status'] = true;
            $response['message'] = 'Deleted';
            $this->set_response($response, 200); // OK (200) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'message' => "Operation failed"
            ], 404); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function like_post() {

        $id = (int) $this->input->post('id', true);
        $ukey = (int) $this->input->post('ukey', true);

        // Validate the id.
        if ($id <= 0 || $ukey <= 0)
        {
            $this->response(array('message' => 'Invalid Parameters'), 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $response = array(
            'status' => false
        );

        $this -> load -> library("Fs_Review" );

        // Validate the id.
        if ($this -> fs_review -> hasLiked( $id, $ukey ))
        {
            $this->response(array('message' => 'You have already liked this review !'), 200); // BAD_REQUEST (400) being the HTTP response code
        }

        $del = $this -> fs_review -> like( $id, $ukey );

        if( $del ) {
            $response['status'] = true;
            $response['message'] = 'Liked';
            $this->set_response($response, 200); // OK (200) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'message' => "Operation failed"
            ], 200); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function edit_post() {

        $ukey = (int) $this->input->post('ukey', true);
        $review_id = (int) $this->input->post('review_id', true);
        $edited_review = $this->input->post('edited_review', true);

        $this -> load -> library('form_validation');

        $this -> form_validation -> set_rules('ukey','ukey','trim|required');
        $this -> form_validation -> set_rules('review_id','review_id','trim|required');
        $this -> form_validation -> set_rules('edited_review','Review','trim|required|min_length[40]');

        if($this -> form_validation -> run() == FALSE)
        {
            $this->response(
                array(
                    'status' => false,
                    'message' => strip_tags( validation_errors() )
                ), 200); // return as 200, in order to display the error message in json
        }

        // Validate the id.
        if ($ukey <= 0 || $review_id <= 0)
        {
            $this->response(array('message' => 'Invalid Parameters'), 400); // BAD_REQUEST (400) being the HTTP response code
        }

        $response = array(
            'status' => false
        );

        $this -> load -> library("Fs_Review", array( 'id' => $review_id ) );

        // Validate the id.
        if ( ! $this -> fs_review -> isOwner( $ukey ) )
        {
            $this->response(array('message' => 'You do own this review !'), 200);
        }

        $del = $this -> fs_review -> edit( $edited_review );

        if( $del ) {
            $response['status'] = true;
            $response['message'] = 'Edit Complete';
            $this->set_response($response, 200); // OK (200) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'message' => "Operation failed"
            ], 404); // NOT_FOUND (404) being the HTTP response code
        }
    }

}
