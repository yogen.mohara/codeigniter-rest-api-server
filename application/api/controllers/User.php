<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class User extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {

        ini_set('display_errors', 1);

        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        $this -> load -> helper("my_helper");

    }

    /*
    change password for the current user
    */
    public function password_post()
    {

        $id = $this->input->post('ukey');
        $pass = $this->input->post('pass');
        $re_pass = $this->input->post('re_pass');
        
        $this -> load -> library('form_validation');

        $this -> form_validation -> set_rules('ukey','ukey','trim|required');
        $this -> form_validation -> set_rules('pass','password','trim|required|min_length[6]');
        $this -> form_validation -> set_rules('re_pass','re_password','trim|required|matches[pass]');

        if($this -> form_validation -> run() == FALSE)
        {
            $this->response(
                array(
                    'status' => false,
                    'message' => strip_tags( validation_errors() )
                ), 200); // return as 200, in order to display the error message in json
        }

        $response = null;

        $this -> load -> library("Fs_User", array('id' => $id) );
        $a = $this -> fs_user -> changePassword( $pass );

        if( $a ) {
            $response = array(
                'status' => true,
                'message' => 'Password Changed'
            );
            $this->set_response($response, 200); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => false,
                'message' => "Operation Failed for user: " . $this -> fs_user -> id
            ], 200); // NOT_FOUND (404) being the HTTP response code
        }
    }


}
