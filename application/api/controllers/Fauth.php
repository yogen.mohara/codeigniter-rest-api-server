<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Fauth extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {

        ini_set('display_errors', 1);

        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['flogin_post']['limit'] = 1000; // 100 requests per hour per user/key

        $this -> load -> helper("my_helper");

        $this -> load -> library("Fs_Auth");
    }

    public function flogin_post()
    {
        if ( empty($this->post('username')) || empty($this->post('password')) )
        {
            $this->set_response([
                'status' => false,
                'message' => 'Incomplete login credentials !'
            ], 400);
            exit();
        }

        $message = [
            'id' => 100, // Automatically generated by the model
            'name' => $this->post('name'),
            'email' => $this->post('email'),
            'message' => 'Added a resource',
            'params' => $this->post()
        ];

        $this->set_response($message, 201); // CREATED (201) being the HTTP response code


        /*

        $username = $this->post('username');
        $password = $this->post('password');

        if ( empty($username) || empty($password) )
        {
            $this->set_response([
                'status' => false,
                'message' => 'Incomplete login credentials !'
            ], 400); // NOT_FOUND (404) being the HTTP response code
            exit();
        }

        $login = $this -> fs_auth -> login($username, $password);

        if( $login['status'] === true ) {
            $message = [
                'status' => 'success',
                'message' => 'Login Success'
            ];
        }
        else {
            $message = [
                'status' => 'fail',
                'message' => $login['message']
            ];
        }
        $this->set_response($message, 200); // OK (200) being the HTTP response code*/
    }

}
