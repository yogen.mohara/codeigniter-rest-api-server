<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model
{

    public function del(string $tbl, array $cond)
    {
        //pre($tbl);
        //pre($cond);
        return $this -> db -> delete($tbl,$cond);
    }

    public function upd(string $tbl, array $data, array $cond)
    {
        $this -> db -> where($cond);

        if ( ! $this -> db -> update($tbl, $data) )
        {
            $error = $this -> db -> error(); // Has keys 'code' and 'message'
            log_message('error: ',$error);
            return false;
        }
        return true;
    }

    public function ins(string $tbl, array $data)
    {
        if ( ! $this -> db -> insert($tbl, $data))
        {

            $error = $this->db->error(); // Has keys 'code' and 'message'
            log_message('error: ',$error);
            return false;
        }
        //echo $this -> db -> last_query();

        return $this -> db -> insert_id();
    }

}