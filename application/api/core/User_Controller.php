<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_Controller extends MY_Controller
{
    protected $data;

    function __construct()
    {

        parent::__construct();

        $this -> validateSession();

        $this->template->set_template('dashboard');

    }

    protected function notif() {

        $this -> load -> library("sl_notification", array(
            "user_account_type" => "user",
            "user_id" => $this -> session -> userdata("logged_user_id"),
        ));

        return $this -> sl_notification -> fetch( array(
            "latest_notif" => true,
            ));
    }

    protected function checkSession()
    {
        if(
            $this -> session -> userdata("is_logged") === true &&
            $this -> session -> userdata("logged_user_id") > 0 &&
            ( $this -> session -> userdata("logged_user_session_type") != 'brand' )
        ) {
            die("Access Denied for invalid user type !!");
        }

        if(
            $this -> session -> userdata("is_logged") === true &&
            $this -> session -> userdata("logged_user_id") > 0 &&
            ( $this -> session -> userdata("logged_user_session_type") == 'brand' )
        )
            return true;
        return false;

    }

    protected function validateSession()
    {
        if( ! $this -> checkSession() )
            redirect( "auth/",'refresh');
    }

    protected function isBrandUser()
    {
        if( $this -> session -> userdata("is_brand_user") === true )
        {
            return true;
        }
        return false;
    }

}