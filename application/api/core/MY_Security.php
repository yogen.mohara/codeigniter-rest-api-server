<?php
class MY_Security extends CI_Security {

    public function __construct()
    {
        parent::__construct();
    }

    public function csrf_show_error()
    {
        // show_error('The action you have requested is not allowed.');  // default code

        // force page "refresh" - redirect back to itself with sanitized URI for security
        // a page refresh restores the CSRF cookie to allow a subsequent login
        header('Location: ' . htmlspecialchars($_SERVER['REQUEST_URI']), TRUE, 200);

        // if ajax request return appropriate message
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $this -> ajax_response['status']        = 'fail';
            $this -> ajax_response['message']       = "Your browser session has expired has expired. Please refresh.";
            $this -> ajax_response['redirect']      = htmlspecialchars($_SERVER['HTTP_REFERER']);

            echo json_encode($this -> ajax_response);exit();
        }
        else {
            header('Location: ' . htmlspecialchars($_SERVER['HTTP_REFERER']), TRUE, 200);
        }
    }
}