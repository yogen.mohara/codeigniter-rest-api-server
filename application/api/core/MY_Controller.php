<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if(ENVIRONMENT == 'development' || isset($_GET['testdata']))
            $this->output->enable_profiler(TRUE);
    }


    protected function checkSession()
    {
        if(
            $this -> session -> userdata("is_logged") === true &&
            $this -> session -> userdata("logged_user_id") > 0
        )
            return true;
        return false;

    }

    protected function checkIfAjax()
    {
        if($this->input->is_ajax_request())
        {
            return true;
        }

        return false;
    }

    /* to be called by controller methods that are ajax in nature
    also profiling needs to be set false in ajax calls */
    protected function checkAjax()
    {
        if($this->input->is_ajax_request())
        {
            $this->output->enable_profiler(FALSE);
            return true;
        }

        die("Invalid Access Method");
    }

    protected function error_403() {
        $err = $this -> load -> view( VIEW_ERROR . "error_403" , array(), true);
        die($err);
    }

}