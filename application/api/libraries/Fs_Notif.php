<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fs_Notif
{

    /* vars */
    protected $CI;

    public $id;
    public $type;
    public $notif;
    public $date;
    public $ukey;
    public $is_read;

    public $user_full_name;

    public function __construct(array $config = array() )
    {
        $this->CI =& get_instance();

        if( isset( $config['ukey'] ) ) $this -> ukey = $config['ukey'];

        $this->CI -> load -> model("notif_model");

    }

    public function getByUser( int $ukey ) {

        $this -> ukey = $ukey;

        $pc = $this -> CI -> notif_model -> getByUser( $this -> ukey );

        return $pc;

    }

}


