<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fs_Auth
{

    /* vars */
    protected $CI;

    private $id;
    private $email;
    private $password;
    private $status;

    private $page = 1;
    private $listing;
    private $rows_per_page = 20;

    public function __construct(array $config = array())
    {
        $this->CI =& get_instance();

        $this->CI -> load -> model("user_model");
    }

    public function login( $username, $password ) {

        $res = array(
            'status' => false,
            'message' => 'Could not complete login request'
        );

        $login = $this -> CI -> user_model -> validateLogin( $username, $password );

        if( ! $login ) {
            return $res;
        }

        if( $login['login'] === true ) {
            $res['status'] = true;
            $res['message'] = 'Login Success';
        }
        else {
            $res['status'] = false;
            $res['message'] = $login['message'];
        }

        return $res;
    }

}


