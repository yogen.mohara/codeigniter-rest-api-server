<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fs_Category
{

    /* vars */
    protected $CI;

    private $id;
    private $category;
    private $primary_category;

    private $page = 1;
    private $listing;
    private $rows_per_page = 20;

    public function __construct(array $config = array())
    {
        $this->CI =& get_instance();

        $this->CI -> load -> model("category_model");

    }

    public function getSecondary() {

        $listing = $this -> CI -> category_model -> getSecondary();

        if( ! $listing ) return false;

        return $listing;
    }

}


