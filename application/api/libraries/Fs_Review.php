<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fs_Review
{

    /* vars */
    protected $CI;

    public $id;
    public $review;
    public $rating;
    public $date;
    public $brand_id;
    public $ukey;
    public $like_count;
    public $is_verified;
    public $verification_token;
    
    public $brand_name;
    public $user_full_name;

    public function __construct(array $config = array() )
    {
        $this->CI =& get_instance();

        if( isset( $config['id'] ) ) $this -> id = $config['id'];
        
        $this->CI -> load -> model("review_model");
        $this->CI -> load -> model("review_like_model");

    }

    public function getUserReviews( int $ukey ) {
        
        $this -> ukey = $ukey;

        $pc = $this -> CI -> review_model -> getUserReviews( $this -> ukey );

        return $pc;

    }

    public function getBrandReviews( int $brand_id ) {

        $this -> brand_id = $brand_id;

        $pc = $this -> CI -> review_model -> getBrandReviews( $this -> brand_id );

        return $pc;

    }

    public function getReviews() {
        $this -> getBrandReview();
        return $this -> reviews;
    }

    public function delete( int $review_id, $ukey ) {

        $this -> id = $review_id;
        $this -> ukey = $ukey;

        if( ! $this -> CI -> review_model -> validateOwner($this -> id,$this -> ukey)) return false;

        $pc = $this -> CI -> review_model -> del( REVIEW, array( 'id' => $this -> id, 'ukey' => $this -> ukey ) );

        return $pc;

    }

    public function like( $review_id, $ukey ) {

        $this -> id = $review_id;
        $this -> ukey = $ukey;

        $pc = $this -> CI -> review_like_model -> ins(
            REVIEW_LIKES,
            array( 'review_id' => $this -> id, 'user_id' => $this -> ukey )
        );

        return $pc;

    }

    public function hasLiked( $review_id, $ukey ) {

        $this -> id = $review_id;
        $this -> ukey = $ukey;

        if( $this -> CI -> review_like_model -> validateOwner($this -> id,$this -> ukey) > 0) return true;
        return false;

    }

    public function edit( $review ) {

        $this -> review = $review;

        $pc = $this -> CI -> review_model -> upd(
            REVIEW,
            array( 'review' => $this -> review ) ,
            array( 'id' => $this -> id )
        );

        return $pc;

    }

    public function isOwner( $ukey ) {

            $this -> ukey = $ukey;

            if( $this -> CI -> review_model -> validateOwner($this -> id, $this -> ukey) > 0)
                return true;

            return false;

    }

}


