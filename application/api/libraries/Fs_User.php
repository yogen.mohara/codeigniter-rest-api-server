<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fs_User
{

    /* vars */
    protected $CI;

    public $id;
    public $email;
    public $password;

    public function __construct(array $config = array() )
    {
        $this->CI =& get_instance();

        if( isset( $config['id'] ) ) $this -> id = $config['id'];

        $this->CI -> load -> model("user_model");

    }

    public function changePassword( string $pass ) {

        $this -> password = $pass;

        $pc = $this -> CI -> user_model -> upd(
            USER,
            array( 'password' => password_hash( $this -> password , PASSWORD_DEFAULT ) ),
            array( 'id' => $this -> id )
        );

        return $pc;

    }

}


