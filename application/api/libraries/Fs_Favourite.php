<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fs_Favourite
{

    /* vars */
    protected $CI;

    public $ukey;
    public $brand_id;
    public $created_on;

    public $brand_name;
    public $user_name;

    public function __construct(array $config = array())
    {
        $this->CI =& get_instance();

        $this->CI -> load -> model("brand_model");
        $this->CI -> load -> model("user_model");
        $this->CI -> load -> model("favourite_model");

    }

    public function getFavouritedBrands( int $user_id ) {

        $this -> ukey = $user_id;

        $listing = $this -> CI -> favourite_model -> getBrandByUser( $this -> ukey );

        if( ! $listing ) return false;

        return $listing;
    }

}


