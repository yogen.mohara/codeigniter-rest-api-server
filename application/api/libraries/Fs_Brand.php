<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fs_Brand
{

    /* vars */
    protected $CI;

    private $id;
    private $name;
    private $about;
    private $policies;
    private $store_location;
    private $address;
    private $contact_phone;
    private $contact_email;
    private $price_category;
    private $city;
    private $shopping_center;
    private $opening_hours;
    private $ukey;
    private $created_on;
    private $status;

    private $assigned_category_ids = array();
    private $assigned_category_labels;

    private $assigned_tagging_ids = array();
    private $assigned_tagging_labels;

    private $price_category_label;

    private $brand_promo;
    private $brand_promo_offer;

    private $brand_sale;
    private $brand_sale_offer;

    private $shopping_center_map;
    private $today_timing;

    private $brand_logo;

    public $reviews_count;
    public $reviews;
    public $rating;

    private $feedback;
    private $comment;
    
    public $logo;
    private $gallery;
    
    private $pm; // stores all form data passed to the lib
    private $open_time_id;

    private $page = 1;
    private $listing;
    private $rows_per_page = 20;

    public function __construct(array $config = array() )
    {
        $this->CI =& get_instance();

        if( isset( $config['id'] ) ) $this -> id = $config['id'];

        $this->CI -> load -> model("brand_model");
        $this->CI -> load -> model("brand_cat_model");
        $this->CI -> load -> model("price_category_model");
        $this->CI -> load -> model("promo_model");
        $this->CI -> load -> model("review_model");
        $this->CI -> load -> model("favourite_model");
        $this->CI -> load -> model("opening_hours_model");

    }

    public function get( int $id ) {

        $this -> id = $id;

        $listing = $this -> CI -> brand_model -> getRow( $this -> id );

        if( ! $listing ) return false;

        $this -> getBrandAssignedPrimaryCategoryLabels();
        $listing['assigned_primary_category_labels'] = $this -> assigned_category_labels;

        $this -> getBrandAssignedTaggingLabels();
        $listing['assigned_tagging_labels'] = $this -> assigned_tagging_labels;

        $this -> price_category = $listing['price_category'];
        $this -> getPriceCategoryLabel();
        $listing['price_category_label'] = $this -> price_category_label;

        $this -> getBrandPromo();
        $listing['brand_sale'] = $this -> brand_sale;
        $listing['brand_sale_offer'] = $this -> brand_sale_offer;
        $listing['brand_promo'] = $this -> brand_promo;
        $listing['brand_promo_offer'] = $this -> brand_promo_offer;

        $this -> shopping_center = $listing['shopping_center'];
        $this -> getShoppingCenterMaps();
        $listing['shopping_center_map'] = $this -> shopping_center_map;

        $this -> getBrandLogo();
        $listing['brand_logo'] = $this -> logo;

        $this -> getBrandGallery();
        $listing['gallery'] = $this -> gallery;

        $this -> getBrandReview();
        // $listing['reviews'] = $this -> reviews;
        $listing['reviews_count'] = $this -> reviews_count;
        $listing['rating'] = $this -> rating;

        $this -> opening_hours = $listing['opening_hours'];
        $this -> getTodayTiming();
        $listing['today_timing'] = $this -> today_timing;

        return $listing;
    }

    public function getBrandAssignedPrimaryCategoryLabels() {

        $cats = $this -> CI -> brand_cat_model -> getAssinedPrimaryCategories( $this -> id );

        $lb = array();
        foreach ( $cats as $cat) {
            $this -> assigned_category_ids[] = $cat['category_id'];
            $lb[] = $cat['category_name'];
        }

        $this -> assigned_category_labels = implode( ", ", $lb );

        return;
    }

    public function getBrandAssignedTaggingLabels() {

        $cats = $this -> CI -> brand_cat_model -> getAssinedTaggings( $this -> id );

        $lb = array();
        foreach ( $cats as $cat) {
            $this -> assigned_tagging_ids[] = $cat['category_id'];
            $lb[] = $cat['category_name'];
        }

        $this -> assigned_tagging_labels = implode( ", ", $lb );

        return;
    }

    public function getPriceCategoryLabel() {

        $pc = $this -> CI -> price_category_model -> getRow( $this -> price_category );

        $this -> price_category_label = $pc['cat'] . ' - ' . $pc['name'];

        return;

    }

    public function getBrandPromo() {

        $pc = $this -> CI -> promo_model -> getBrandPromo( $this -> id );

        if( ! $pc ) return;

        $this -> brand_sale = $pc['sale_promo_flag'] == '1' ? 'true' : 'false';
        $this -> brand_sale_offer = $pc['sale_promo_flag'] == '1' ? $pc['offer_desc'] : null;

        $this -> brand_promo = $pc['sale_promo_flag'] == '2' ? 'true' : 'false';
        $this -> brand_promo_offer = $pc['sale_promo_flag'] == '2' ? $pc['offer_desc'] : null;

        return;

    }

    public function getShoppingCenterMaps() {

        $ar_shopping_center_gmap = array(
            "Deira City Center" => "<iframe id=\"gmap_canvas\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3608.460968429606!2d55.328121114328546!3d25.255074235549035!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5cd8dc6278e7%3A0xdcd0fc0c457ce675!2sDeira+City+Centre!5e0!3m2!1sen!2sin!4v1556640629020!5m2!1sen!2sin\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>",
            "Mall of the Emirates" => "<iframe id=\"gmap_canvas\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3612.498190364859!2d55.2001465143255!3d25.1188426410406!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6bbf14ecb6af%3A0x637f6b87f08b83c7!2sMall+of+the+Emirates!5e0!3m2!1sen!2sin!4v1556640581139!5m2!1sen!2sin\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>",
            "Dubai Festival City" => "<iframe id=\"gmap_canvas\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28876.07575462694!2d55.341879242653945!3d25.219759855525872!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f677ee7aee0fd%3A0x9ca6e961d7706272!2sDubai+Festival+City+-+Dubai+-+United+Arab+Emirates!5e0!3m2!1sen!2sin!4v1556640515692!5m2!1sen!2sin\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>",
            "Burjuman" => "<iframe id=\"gmap_canvas\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3608.471393562199!2d55.30208301432859!3d25.25472333556311!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f432febf51e1b%3A0x64662d6ca066ade1!2sBurjuman!5e0!3m2!1sen!2sin!4v1556640672571!5m2!1sen!2sin\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>",
            "Wafi Mall" => "<iframe id=\"gmap_canvas\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3609.1966294400263!2d55.31849826619567!3d25.230301397300114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5d35f0425a33%3A0x4ca1b0dbf1d7981b!2sWAFI+Mall!5e0!3m2!1sen!2sin!4v1556640729206!5m2!1sen!2sin\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>",
            "Mercato" => "<iframe id=\"gmap_canvas\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3609.607467413207!2d55.250797914327684!3d25.216456837108446!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f42414d7e336f%3A0xc8941b8795e15450!2sMercato+Shopping+Mall!5e0!3m2!1sen!2sin!4v1556640776968!5m2!1sen!2sin\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>",
            "Dubai Mall" => "<iframe id=\"gmap_canvas\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7220.342445543365!2d55.275120522420494!3d25.197447643556096!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f682829c85c07%3A0xa5eda9fb3c93b69d!2sThe+Dubai+Mall!5e0!3m2!1sen!2sin!4v1556640808545!5m2!1sen!2sin\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>",
        );

        if( key_exists( $this -> shopping_center, $ar_shopping_center_gmap ) ) {
            $this -> shopping_center_map = $ar_shopping_center_gmap[ $this -> shopping_center ];
        }

        return;

    }

    public function getBrandLogo() {

        $img_location = PATH_BRAND_IMG . $this -> id . "/logo.jpg";

        $this -> logo = $this -> CI -> config -> item("base_url_desk") . "images/slider/1.jpg";

        if ( file_exists( $img_location ) ) {
            $this -> logo = URL_IMG . $this -> id . "/logo.jpg";
        }

        return;

    }

    public function getBrandGallery() {

        // default if no gallery is uploaded for this brand
        $gallery_images = array(
            "assets/img/gallery1.jpg",
            "assets/img/gallery2.jpg",
            "assets/img/gallery3.jpg",
            "assets/img/gallery4.jpg",
        );

        $gallery_brand = array();

        $gallery_folder = PATH_BRAND_IMG . $this -> id . "/gallery";

        if( is_dir ( $gallery_folder ) ) {

            if ( $handle = opendir ( $gallery_folder ) ) {

                while ( false !== ( $entry = readdir ( $handle ) ) ) {

                    if ( $entry != "." && $entry != ".." ) {

                        $gallery_brand[] = URL_IMG . $this -> id . "/gallery/" . $entry;
                    }
                }

                closedir($handle);
            }
        }
        if( count( $gallery_brand ) > 0) $gallery_images = $gallery_brand;

        $this -> gallery = $gallery_images;
    }

    public function getBrandReview() {

        $pc = $this -> CI -> review_model -> getBrandReviews( $this -> id );


//        mail("yogen.mohara@gmail.com", "db", serialize($this -> id));

        if( ! $pc ) return;

        $this -> reviews_count = count ( $pc );
        $this -> reviews = $pc;

        $this -> calcAvgRating();

        return;

    }

    public function calcAvgRating() {
        $total_rating = 0;
        $count_rating = 0;
        foreach ( $this -> reviews as $review) {

            if ( $review['rating'] >= 0 ) {
                $total_rating += $review['rating'];
                $count_rating++;
            }

            $this -> rating = round( ( $total_rating / $count_rating ), 1 );

        }
        return;
    }

    public function getReviews() {
        $this -> getBrandReview();
        return $this -> reviews;
    }

    /* check if a brand is favourited by given user id */
    public function checkFavourited($ukey) {

        $pc = $this -> CI -> favourite_model -> getRow( $ukey, $this -> id );
        return $pc;

    }

    public function getTodayTiming() {

        $pc = $this -> CI -> opening_hours_model -> getRow( $this -> opening_hours );

        if( ! $pc ) return;

        $today = date('D'); // eg: Mon
        $today_open = '';
        $today_close = '';

        switch ( strtolower($today) ) {

            case 'mon':
                $today_open = $pc['open_mon'];
                $today_close = $pc['close_mon'];
                break;

            case 'tue':
                $today_open = $pc['open_tues'];
                $today_close = $pc['close_tues'];
                break;

            case 'wed':
                $today_open = $pc['open_wed'];
                $today_close = $pc['close_wed'];
                break;

            case 'thu':
                $today_open = $pc['open_thurs'];
                $today_close = $pc['close_thurs'];
                break;

            case 'fri':
                $today_open = $pc['open_fri'];
                $today_close = $pc['close_fri'];
                break;

            case 'sat':
                $today_open = $pc['open_sat'];
                $today_close = $pc['close_sat'];
                break;

            case 'sun':
                $today_open = $pc['open_sun'];
                $today_close = $pc['close_sun'];
                break;

            default:
                break;

        }

        $today_timing = $today_open . ' - ' . $today_close;

        if( $today_open == 'Closed' || $today_close == 'Closed' ) {
            $today_timing = 'Closed';
        }

        $this -> today_timing = $today_timing;

        return;
    }

}


